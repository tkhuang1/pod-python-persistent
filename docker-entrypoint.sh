#!/usr/bin/env bash

if [ -n "$PASSWORD" ]; then
	echo "root:$PASSWORD" | chpasswd
fi

#service ssh start
/etc/init.d/ssh start
status=$?
if [ $status -ne 0 ]; then
	echo "Failed to start SSH Server: $status"
	exit $status
fi

# Restore python environment
if [ -d "/opt/conda_bk/" ]; then
	rsync -avzP /opt/conda_bk/ /opt/conda
	status=$?
	if [ $status -ne 0 ]; then
		echo "Failed to restore python environment: $status"
		exit $status
	fi
fi

source /etc/bash.bashrc && jupyter lab --notebook-dir=/home --ip 0.0.0.0 --no-browser --allow-root &
status=$?
if [ $status -ne 0 ]; then
	echo "Failed to start jupyterlab: $status"
	exit $status
fi

# backup_python_envs() {
# 	# backup python environments
# 	rsync -avzP /opt/conda/ /opt/conda_bk/
# }

# sigterm_handler() {
# 	backup_python_envs
# 	exit 143 # 128 + 15 -- SIGTERM
# }

## Setup signal trap
# on callback execute the specified handler
# trap 'sigterm_handler' SIGTERM

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds
while sleep 60; do
	pgrep ssh &>/dev/null
	SSH_STATUS=$?
	pgrep jupyter &>/dev/null
	JUPYTER_STATUS=$?
	# If the greps above find anything, they exit with 0 status
	# If they are not both 0, then something is wrong
	if [ $SSH_STATUS -ne 0 ] || [ $JUPYTER_STATUS -ne 0 ]; then
		echo "One of the processes has already exited."
		# backup_python_envs
		exit 1
	fi
done
