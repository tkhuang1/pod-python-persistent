# syntax=docker/dockerfile:1.4

FROM pytorch/pytorch:1.10.0-cuda11.3-cudnn8-devel

# Fix nvidia stupid key rorate issue, https://github.com/NVIDIA/nvidia-docker/issues/1631
RUN rm /etc/apt/sources.list.d/nvidia-ml.list
RUN apt-key del 7fa2af80
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/3bf863cc.pub
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1807/x86_64/7fa2af80.pub1.0-cudnn8-devel-ubuntu18.04

# Set locale
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

ENV DEBIAN_FRONTEND=noninteractive


RUN rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache

# Add Tini
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

RUN --mount=type=cache,sharing=locked,target=/var/cache/apt --mount=type=cache,sharing=locked,target=/var/lib/apt \
    sed -i 's/archive.ubuntu.com/free.nchc.org.tw/g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends vim wget openssh-server ca-certificates rsync \
    && rm -rf /var/lib/apt/lists/*

RUN --mount=type=cache,sharing=locked,target=/var/cache/apt --mount=type=cache,sharing=locked,target=/var/lib/apt \
    wget --quiet -O - https://raw.github.com/obspy/obspy/master/misc/debian/public.key | apt-key add - \
    && echo deb http://deb.obspy.org xenial main >> /etc/apt/sources.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends python3-obspy \
    && rm -rf /var/lib/apt/lists/*

# SSH
RUN mkdir /var/run/sshd \
    && sed -i 's/#\?PermitRootLogin[[:space:]]\+.*/PermitRootLogin yes/' /etc/ssh/sshd_config \
    && sed -i 's/#\?PasswordAuthentication[[:space:]]\+.*/PasswordAuthentication yes/' /etc/ssh/sshd_config
# SFTP
RUN sed -i 's/#\?Subsystem[[:space:]]\+sftp[[:space:]]\+.*/Subsystem sftp internal-sftp/' /etc/ssh/sshd_config
###

# SSH login fix. Otherwise user is kicked off after login
RUN sed -i 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' \
    /etc/pam.d/sshd
ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile
EXPOSE 22
###

RUN --mount=type=cache,sharing=locked,target=/var/cache/apt --mount=type=cache,sharing=locked,target=/var/lib/apt \
    apt-get update \
    && apt-get install -y --no-install-recommends \
    build-essential \
    git \
    libgoogle-glog-dev \
    libgtest-dev \
    libiomp-dev \
    libleveldb-dev \
    liblmdb-dev \
    libopencv-dev \
    libopenmpi-dev \
    libsnappy-dev \
    libprotobuf-dev \
    openmpi-bin \
    openmpi-doc \
    protobuf-compiler \
    python-dev \
    python-pip \
    libgflags-dev \
    cmake \
    && rm -rf /var/lib/apt/lists/*

ARG PYPI_HOST=10.16.1.5
ARG PYPI_PORT=8080
RUN python -m pip install --index-url http://${PYPI_HOST}:${PYPI_PORT}/ --trusted-host ${PYPI_HOST} gemini-job-dispatcher==0.1.2

# python package
RUN --mount=type=cache,sharing=locked,target=/root/.cache \
    python -m pip install --upgrade pip \
    && python -m pip install \
    pycuda==2021.1 \
    matplotlib==3.5.0 \
    pandas==1.3.4 \
    keras==2.3.1 \
    'ipython>=7,<8' \
    jupyterlab==3.4.2 \
    jupyterlab-system-monitor==0.8.0 \
    jupyterlab_nvdashboard==0.6.0 \
    protobuf==3.20.1 \
    future==0.18.2

ENV HOME=/root

COPY --link jupyter_notebook_config.py ${HOME}/.jupyter/jupyter_notebook_config.py
COPY --link ./docker-entrypoint.sh /docker-entrypoint.sh

EXPOSE 8888

WORKDIR /home

ENTRYPOINT ["/tini", "--", "/docker-entrypoint.sh" ]